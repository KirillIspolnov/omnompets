package net.kivitechnologies.OmNomPets;

/**
 * Класс представляющий котиков
 *   
 * @author Испольнов Кирилл, 16ит18К
 */
public class Cat extends Pet {
    
    /**
     * Конструктор
     * Создает котика с заданной кличкой
     * 
     * @param name кличка котика
     */
    public Cat(String name) {
        super(name);
    } 

    @Override
    public int countHungry(int effortCofficient) {
        return effortCofficient * 7;
    }

    @Override
    public void say(String message) {
        System.out.printf("Котик по кличке \"%s\" передает сообщение: %s%n", getName(), message);
    }

    @Override
    public String toString() {
        return String.format("Котик по кличке %s голоден на %d процентов", getName(), getHungry());
    }
}
