package net.kivitechnologies.OmNomPets;

/**
 * Класс, представляющий домашнего питомца
 * Дочерний класс должен определить два метода:
 * <ul>
 * <li>countHungry</li>
 * <li>say</li>
 * </ul>
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public abstract class Pet {
    private static final String PET_IS_DEAD_FORMAT = "К сожалению, питомец по кличке %s умер от голода...%n";
    
    //Голод питомца
    private byte hungry;
    //Кличка питомца
    private String name;
    
    /**
     * Конструктор
     * Создает питомца с определенной кличкой name
     * 
     * @param name кличка создаваемого питомца
     */
    public Pet(String name) {
        this.name = name;
        this.hungry = 0;
    }
    
    /**
     * Возвращает текущий уровень голода питомца
     * 
     * @return голод питомца в диапазон 0..100
     */
    public byte getHungry() {
        return hungry;
    }
    
    /**
     * Возвращает имя питомца
     *  
     * @return имя питомца
     */
    public String getName() {
        return name;
    }
    
    /**
     * Находит уровень голода полученный от усилий питомца
     * 
     * @param effortCofficient коэффициент усилий
     * @return уровень голода, полученный от усилий
     */
    public abstract int countHungry(int effortCofficient);
    
    /**
     * Говорительный метод
     * Произносит слова, сказанные питомцем
     * 
     * @param message сообщение от питомца
     */
    public abstract void say(String message);
        
    /**
     * Голодательный метод
     * Увеличивает голод на опредленное количество процентов
     * Если голод приближается к 100 %, то выводитпредупредительное сообщение
     * Если голод перевалит за 100 %, то выводит сообщение о смерти питомца
     * 
     * @param hungry уровень, на который голод должен быть увеличен
     */
    private void starve(int hungry) {
        this.hungry += hungry;
        if(this.hungry > 90) {
            say("Ой, спасите - помогите, умираю...");
        } else if(this.hungry > 100) {
            System.out.printf(PET_IS_DEAD_FORMAT, name);
        }
    }
    
    /**
     * Спательный метод
     * Во время сна питомец тоже голодает
     */
    public void sleep() {
        say("Уря! Идем спатеньки!");
        starve(countHungry(1));
    }
    
    /**
     * Гулятельный метод
     * Во время прогулки кот очень сильно может проголодаться
     * Не забудьте его покормить!
     */
    public void walk() {
        say("Я иду гулять!");
        if(Math.random() < 0.5) {
            say("Но там же холодно!");
            starve(countHungry(4));
        } else {
            say("Ураааааа!");
            starve(countHungry(2));
        }
    } 
    
    /**
     * Кушательный метод
     * Снижает уровень голода за счет еды food
     * 
     * @param food еда для питомца
     */
    public void eat(Food food) {
        if(food.canFeed()) {
            say("Ом-ном-ном");
            
            hungry -= food.use(hungry);
            
            if(hungry == 0) {
                say("Ой как вкусно! Я наелся!");
            } else {
                say("Вкусненько! А где еще?");
            }
        } else {
            say("Ой! Тут пусто! Где кушать? Сейчас как кусь!");
        }
    }
}
