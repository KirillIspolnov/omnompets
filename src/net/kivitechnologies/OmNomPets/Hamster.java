package net.kivitechnologies.OmNomPets;

/**
 * Класс представляющий хомяка
 *   
 * @author Испольнов Кирилл, 16ит18К
 */
public class Hamster extends Pet {

    /**
     * Конструктор
     * Создает хомяка с заданной кличкой
     * 
     * @param name кличка хомяка
     */
    public Hamster(String name) {
        super(name);
    }
    
    @Override
    public int countHungry(int effortCofficient) {
        return effortCofficient * 3;
    }

    @Override
    public void say(String message) {
        System.out.printf("Хомяк по кличке \"%s\" передает *ОМ-НОМ* извините, сообщение: %s%n", getName(), message);
    }
 
    @Override
    public String toString() {
        return String.format("Хомяк по кличке %s голоден на %d ", getName(), getHungry());
    }
}
