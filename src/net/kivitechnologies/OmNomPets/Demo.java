package net.kivitechnologies.OmNomPets;

public class Demo {
 
    private static final String SEPARATOR = "*******************************************************";

    /**
     * Без комментариев
     * 
     * @param args ...
     */
    public static void main(String[] args) {
        Pet cat = new Cat("Мурзик"),
            dog = new Dog("Оля"),
            hamster = new Hamster("Жора");
        
        cat.sleep();
        dog.sleep();
        hamster.sleep();
        
        showPetsInfo(cat, dog, hamster);
        
        cat.walk();
        dog.walk();
        hamster.walk();
        
        showPetsInfo(cat, dog, hamster);
        
        Food food = new Food(Food.MAX_RICHNESS);
        cat.eat(food);
        dog.eat(food);
        hamster.eat(food);
        
        showPetsInfo(cat, dog, hamster);
    }  
    
    /**
     * Выводит информацию о каждом из питомцев на отдельной строке
     * 
     * @param cat     питомец-котик
     * @param dog     питомец-песик
     * @param hamster питомец-хома
     */
    public static void showPetsInfo(Pet cat, Pet dog, Pet hamster) {
        System.out.println(SEPARATOR);
        System.out.println(cat);
        System.out.println(dog);
        System.out.println(hamster);
        System.out.println(SEPARATOR);
    }
}
