package net.kivitechnologies.OmNomPets;

/**
 * Класс представляющий песиков
 *   
 * @author Испольнов Кирилл, 16ит18К
 */
public class Dog extends Pet {

    /**
     * Конструктор
     * Создает песика с заданной кличкой
     * 
     * @param name кличка песика
     */
    public Dog(String name) {
        super(name);
    }
    
    @Override
    public int countHungry(int effortCofficient) {
        return effortCofficient * 10; 
    }
 
    @Override
    public void say(String message) {
        System.out.printf("Песик по кличке \"%s\" передает сообщение: %s%n", getName(), message);
    }

    @Override
    public String toString() {
        return String.format("Песик по кличке %s голоден на %d процентов", getName(), getHungry());
    }
}
