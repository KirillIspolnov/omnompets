package net.kivitechnologies.OmNomPets;

/**
 * Класс, представляющий еду для питомцев
 * Единственная характеристика еды - ее сытность
 * 
 * @author Испольнов Кирилл, 16ит18К
 */
public class Food {
    public static final int MAX_RICHNESS = 500;
     
    /**
     * Текущая "сытность" еды
     * Другими словами, это то, насколько может снизиться голод питомца
     */
    private int richness;
    
    /**
     * Конструктор
     * Создает еду с заданной сытностью
     * Если сытность окажется больше максимальной, то объект получит максимально возможную сытность
     * 
     * @param richness сытность еды
     */
    public Food(int richness) {
        this.richness = Math.min(richness, MAX_RICHNESS);
    }
    
    /**
     * Возвращает true, если еда все еще может утолить голод питомца, иначе - false
     * 
     * @return true, если еда все еще может утолить голод питомца, иначе - false
     */
    public boolean canFeed() {
        return richness > 0;
    }
    
    /**
     * Позволяет использовать еду для кормления питомца
     * 
     * @param hungry текущий голод питомца
     * @return уровень, на который голод питомца нужно снизить
     */
    public int use(int hungry) {
        return Math.min(hungry, richness);
    }
}
